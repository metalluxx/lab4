#include "containslist.h"

ContainsList::ContainsList()
{
    this->setObjectName(QStringLiteral("saveList"));
    this->setMinimumSize(QSize(350, 0)); // установка фиксированной ширины
    this->setMaximumSize(QSize(350, 16777215));
}


bool QListWidgetItem::operator<(const QListWidgetItem &right)const{
    bool accept = false;
    containsCategory ct2 = qvariant_cast<containsCategory>(right.data(CONTAINS_CATEGORY)); //преобразование QVariant в enum-значения
    containsCategory ct1 = qvariant_cast<containsCategory>(this->data(CONTAINS_CATEGORY));
    containsPeriod cp2 = qvariant_cast<containsPeriod>(right.data(CONTAINS_PERIOD));
    containsPeriod cp1 = qvariant_cast<containsPeriod>(this->data(CONTAINS_PERIOD));
    QString cs1 = this->data(CONTAINS_NAME).toString();
    QString cs2 = right.data(CONTAINS_NAME).toString();

    if(cs1 == cs2) // условия сравнения
    {
        if(ct1 == ct2)
        {
            if(cp1  == cp2)
            {
                accept = false;
            }
            else accept =  (cp1  < cp2);
        }
        else  accept =  (ct1 < ct2);
    }
    else accept =  (cs1 < cs2);

    return accept;

}
