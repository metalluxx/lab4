#include <QtCore>
#include <QtGui>
#include <QtWidgets>

#ifdef Q_OS_MAC
#include <QtMacExtras>
#include <QMacToolBar>
#endif

class Wrapper
{
private:
    Wrapper(){}
    Wrapper(const Wrapper&);
    Wrapper& operator=(Wrapper&);
public:
    static Wrapper& getInstance() {
        static Wrapper instance;
        return  instance;
    }

public slots:
    void emitClick();
    void textInDock(QString kek);
    void setLabelToolBar(QMacToolBar* kek);
signals:
    void dockClicked();
};
