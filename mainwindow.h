#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#define CONTAINS_ID 1001
#define CONTAINS_CATEGORY 1002
#define CONTAINS_PERIOD 1003
#define CONTAINS_NAME 1004

#include <QMainWindow>
#include "contains.h"
#include "containsctl.h"
#include <QtCore>
#include <QtGui>
#include <QtWidgets>
#include <MacWrapper.h>


#ifdef Q_OS_MAC
#include <QtMacExtras>
#include <QMacToolBar>
#endif

namespace Ui {
class MainWindow;
}
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    //Изменение видимости виджетов для критериев
    void on_cJur_pressed();
    void on_cGaz_pressed();
    void uncheckAllPer();

    //Методы чтения и записи
    void writeData(Contains *tmp);
    void readData(Contains *tmp);

    // 4 слота интерфейса
    void on_numberSlider_sliderMoved(int position);
    void on_moneySlider_sliderMoved(int position);
    void on_numberId_valueChanged(int arg1);
    void on_moneycount_valueChanged(double arg1);

    //Слоты для работы с итемами
    void on_itemDelete_triggered();
    void on_itemAdd_triggered();
//    void on_itemSave_triggered();
    void on_saveList_currentRowChanged(int currentRow);
    void updateList(int idRow);

    // Слоты для взаимодействия с файлами
    void on_listLoad_triggered();
    void on_listWrite_triggered();

    // Кнопка выхода
    void on_clCancel_clicked();
    void closeEvent(QCloseEvent *cEvent);

    void savechanges();
    void enableui(bool en);

    void on_nameId_textChanged(const QString &arg1);

    void on_cJur_clicked();

    void on_cGaz_clicked();

    void on_numberId_valueChanged(const QString &arg1);

    void on_numberSlider_valueChanged(int value);

    void on_perEday_clicked();

    void on_perW1_clicked();

    void on_perM2_clicked();

    void on_perM1_clicked();

    void on_perY6_clicked();

    void on_perY2_clicked();

    void on_perY4_clicked();

    void on_perY1_clicked();

    void on_fInfo_clicked();

    void on_fModa_clicked();

    void on_fSport_clicked();

    void on_fHel_clicked();

    void on_fSad_clicked();

    void on_fTravel_clicked();

    void on_fTV_clicked();

    void on_checkBox_toggled(bool checked);

    void on_dateEdit_userDateChanged(const QDate &date);

private:
    ContainsCtl *listData = new ContainsCtl;
    bool sch_read = true;
    bool sl_read = true;
    bool loadfile = true;
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
