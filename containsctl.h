#ifndef CONTAINSCTL_H
#define CONTAINSCTL_H

#include "contains.h"
#include <QtCore>
#include <QtWidgets>

#define CONTAINS_ID 1001
#define CONTAINS_CATEGORY 1002
#define CONTAINS_PERIOD 1003
#define CONTAINS_NAME 1004

class ContainsCtl
{
public:
    ContainsCtl();
    int count() const;
    int append(Contains &record);
    void remove(int id);
    int update(const Contains &record);
    void record(int id, Contains &record);
    bool save(QString filename);
    bool load(QString filename);
    void clear();
    QList<QListWidgetItem>* listItem();
    QListWidgetItem* item(int id);
    int getCorrectRandomId();

private:

    QLinkedList<Contains> *containsList = new QLinkedList<Contains>;
    QStringList* containsToString(); //
};

#endif // CONTAINSCTL_H
