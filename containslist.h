#if ! defined (CONTAINS_H) // стражи включения для enum
#include <QVariant>
enum containsCategory {CNULL, JURNAL, GAZETA, };
enum containsPeriod {PNULL, EVERYDAY, WEAK1, MONTH2, MONTH1, YEAR6, YEAR4, YEAR2, YEAR1 };
enum containsFormat {FNULL, INFO, MODA, SPORT, HEALTH, GARDEN, TRAVEL, TV};
Q_DECLARE_METATYPE(containsCategory); // добавление enum's в QVariant
Q_DECLARE_METATYPE(containsPeriod);
Q_DECLARE_METATYPE(containsFormat);
#endif

#ifndef CONTAINSLIST_H
#define CONTAINSLIST_H

#define CONTAINS_ID 1001 // глобальные константы
#define CONTAINS_CATEGORY 1002
#define CONTAINS_PERIOD 1003
#define CONTAINS_NAME 1004

#include <QListWidget>
#include <QListWidgetItem>
#include <QVariant>

class ContainsList : public QListWidget
{
public:
    ContainsList();
    // переопределение оператора сортировки для списка
    friend bool QListWidgetItem::operator<(const QListWidgetItem &rigth)const;
};


#endif // CONTAINSLIST_H
