#import "MacWrapper.h"
#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>


void dockClickHandler(id self, SEL _cmd)
{
    Q_UNUSED(self)
    Q_UNUSED(_cmd)
    Wrapper::getInstance().emitClick();
}

void Wrapper::emitClick()
{
    emit dockClicked();
}

void Wrapper::textInDock(QString kek){
    NSString* sas = kek.toNSString();
    [[NSApp dockTile] setBadgeLabel: sas];
}


void Wrapper::setLabelToolBar(QMacToolBar* kek){

    NSToolbar* sas = kek->nativeToolbar();
    [sas setDisplayMode:NSToolbarDisplayModeLabelOnly];
}
