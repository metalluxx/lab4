#include "containsctl.h"

ContainsCtl::ContainsCtl()
{
}

int ContainsCtl::count() const{
    return containsList->size();
}

int ContainsCtl::append( Contains &record){
    Contains tmp = record;
    tmp.id = getCorrectRandomId();
    containsList->append(tmp);
    return tmp.id;
}

bool ContainsCtl::save(QString filename){
    QFile *file = new QFile(filename);
    if(!file->open(QIODevice::ReadWrite)) return false;
    QStringList *list = containsToString(); // получение текста с информацией о текущих элементах контейнера
    QTextStream stream(file);
    foreach(QString tmp, *list) // запись в файл
    {
        stream << tmp << "\n";
    }
    file->close(); // завершение операций с файлом
    delete file;
    delete list;
    return true;
}

bool ContainsCtl::load(QString filename){
    QFile *file = new QFile(filename);
    if(!file->open(QIODevice::ReadOnly)) return false; // если файл не открылся
    QTextStream stream(file);
    containsList->clear(); // чистка контейнера
    while (!stream.atEnd()) // парсинг файла
    {
        QString tmp = stream.readLine();
        if(tmp == "CONTAINS_BEGIN"){
            Contains containsTmp; // предварительное создание элемента и последующее его заполение информацией из файла
            containsTmp.id = stream.readLine().toInt();
            containsTmp.cd = stream.readLine().toInt();
            containsTmp.money = stream.readLine().toDouble();
            containsTmp.num = stream.readLine().toInt();
            containsTmp.date = QDate::fromString(stream.readLine(),"dd-MM-yyyy");
            containsTmp.name = stream.readLine();
            switch (stream.readLine().toInt()) {
            case 0: containsTmp.cat = containsCategory::CNULL; break;
            case 1: containsTmp.cat = containsCategory::JURNAL; break;
            case 2: containsTmp.cat = containsCategory::GAZETA; break;
            }
            switch (stream.readLine().toInt()) {
            case 0: containsTmp.per = containsPeriod::PNULL; break;
            case 1: containsTmp.per = containsPeriod::EVERYDAY; break;
            case 2: containsTmp.per = containsPeriod::WEAK1; break;
            case 3: containsTmp.per = containsPeriod::MONTH2; break;
            case 4: containsTmp.per = containsPeriod::MONTH1; break;
            case 5: containsTmp.per = containsPeriod::YEAR6; break;
            case 6: containsTmp.per = containsPeriod::YEAR4; break;
            case 7: containsTmp.per = containsPeriod::YEAR2; break;
            case 8: containsTmp.per = containsPeriod::YEAR1; break;
            }
            switch (stream.readLine().toInt()) {
            case 0: containsTmp.format = containsFormat::FNULL; break;
            case 1: containsTmp.format = containsFormat::INFO; break;
            case 2: containsTmp.format = containsFormat::MODA; break;
            case 3: containsTmp.format = containsFormat::SPORT; break;
            case 4: containsTmp.format = containsFormat::HEALTH; break;
            case 5: containsTmp.format = containsFormat::GARDEN; break;
            case 6: containsTmp.format = containsFormat::TRAVEL; break;
            case 7: containsTmp.format = containsFormat::TV; break;
            }
            containsList->append(containsTmp); // вставка данного элемента в контейнер
        }
    }
    delete file;
    return true;
}

QStringList* ContainsCtl::containsToString(){
    if(containsList->size() == 0) // если пусто в контейнере
    {
        QStringList *tmpList = new QStringList;
        *tmpList << "CONTAINS_NULL";
        return tmpList;
    }
    QStringList *tmpList = new QStringList; // текст будущего файла

    for(QLinkedList<Contains>::const_iterator itr = containsList->constBegin(); itr != containsList->end(); itr++) // заполнение текст. информацией об элементах контейнера
    {
        *tmpList << "CONTAINS_BEGIN";
        *tmpList << QString::number(itr->id);
        *tmpList << QString::number(itr->cd);
        *tmpList << QString::number(itr->money);
        *tmpList << QString::number(itr->num);
        *tmpList << itr->date.toString("dd-MM-yyyy");
        *tmpList << itr->name;
        *tmpList << QString::number(itr->cat);
        *tmpList << QString::number(itr->per);
        *tmpList << QString::number(itr->format);
        *tmpList << "CONTAINS_END";
        *tmpList << "";
    }
    return  tmpList;
}

QList<QListWidgetItem>* ContainsCtl::listItem(){
    QList<QListWidgetItem> *tmpList = new QList<QListWidgetItem>;
    QList<QLinkedList<Contains>::iterator> *tmp_itr = new QList<QLinkedList<Contains>::iterator>; // временный контейнер из итераторов, которые будут отсортированы для списка

    for(QLinkedList<Contains>::iterator tmp = containsList->begin(); tmp != containsList->end(); tmp++) // заполним tmp_itr итераторами основного контейнера
    {
        tmp_itr->append(tmp);
    }
    std::sort(tmp_itr->begin(), tmp_itr->end(), [](QLinkedList<Contains>::iterator t1, QLinkedList<Contains>::iterator t2){
        if(t1->cat == t2->cat){
            if(t1->per == t2->per){
                if(t1->num == t2->num){
                    return false;
                }else return t1->num < t2->num;
            }else return  t1->per < t2->per;
        }else return t1->cat < t2->cat;
    }); // и сортируем временный контейнер

    foreach(QLinkedList<Contains>::iterator sorted_contains, *tmp_itr) // заполняем интерфейс данными из уже отсортированного контейнера
    {
        QListWidgetItem*it = new QListWidgetItem(); // предварительное создание итема
        it->setData(CONTAINS_ID, sorted_contains->id);
        it->setText(QString(sorted_contains->name + " | " + QString::number(sorted_contains->money, 'f', 2 )));
        tmpList->append(*it); // и добавление этого итема в список
        delete it;
    }
    delete tmp_itr;
    return  tmpList;
}

QListWidgetItem* ContainsCtl::item(int id){
    for(QLinkedList<Contains>::const_iterator itr = containsList->constBegin();itr != containsList->end(); itr++){
        if(id == itr->id){
            QListWidgetItem*it = new QListWidgetItem(); // предварительное создание итема
            it->setData(CONTAINS_ID, itr->id);
            it->setText(QString(itr->name + " | " + QString::number(itr->money, 'f', 2 )));
            return it; // и добавление этого итема в список
        }
    }
    return nullptr;
}

void ContainsCtl::remove(int id){
    for(QLinkedList<Contains>::iterator itr = containsList->begin();itr != containsList->end();itr++){
        if(itr->id == id){
            containsList->erase(itr);
            break;
        }
    }
}

int ContainsCtl::getCorrectRandomId() // рандомайзер id
{
    int random = rand();
    foreach(Contains correct, *containsList){
        if(random == correct.id) getCorrectRandomId();
    }
    return random;
};

int ContainsCtl::update(const Contains& record){
    for(QLinkedList<Contains>::iterator itr = containsList->begin() ;itr != containsList->end();itr++){
        if(itr->id == record.id){
            *itr = record;
            return record.id;
        }
    }
    return -1;
}

void ContainsCtl::record(int id, Contains &record){
    for(QLinkedList<Contains>::iterator itr = containsList->begin() ;itr != containsList->end();itr++){
        if(itr->id == id){
            record = *itr;
            break;
        }
    }
}

void ContainsCtl::clear(){
    containsList->clear();
}
